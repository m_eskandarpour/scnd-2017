#pragma once


#include <ilcplex/ilocplex.h>

class cplexmod
{
public:
	cplexmod(void);
	~cplexmod(void);

	cplexmod(int num1, int& num2,int num3);
	void mathMod();
	bool solveMod(int num1, int num2,int num3);
	void Vardifinition();
	void Constraints();
	void objectivefunction();
	void datareading1array(int*& array1, int quantity);
	void datareading2array(int**& array2, int quantity1, int quantity2);
	void datareading3array(int****& array3, int quantity1, int quantity2, int quantity3, int quantity4);
	void int_to_ascii(int value, char *str);
	void makestring(char *filename,char *ss,int mo, char nam[11]);
	void convert(int num1, int num2);
	void convertResult(int num1, int num2,int num3);
	void testsize(int num1, int& num2);

	

private:
	IloEnv env;
	IloModel* model;
	IloExpr* objf;
	IloIntVar** w;
	IloIntVar** h;
	IloIntVar**** A;
	IloIntVar**** B;
	IloIntVar**** C;
	IloFloatVar***** r;
	IloFloatVar***** x;
	IloFloatVar***** y;
	int supplier;
	int plant;
	int distribution;
	int customer;
	int transportation;
	int ud;
	int up;
	int product;
	float epsilon;
	int** demand;
    int* cas;
    int* cap;
    int* cad;
    int* fcp;
    
    int* fcd;
    
    int* fep;
    int* vep;
    int* fed;
    int* ved;
	int**** tcp;
	int**** tcd;
	int**** tcc;
	int**** cep;
	int**** ced;
	int**** cec;
	char filename[100];
	
	float** DistanceSuppliers;
	float** DistancePlants;
	float** DistanceDistributions;
	float** DistanceCustomers;
	float** pointcustomer;
	float** pointdistribution;
	float** pointplant;
	float** pointsupplier;

	IloExpr* fixedcost;
	IloExpr* variablecost;
	IloExpr* emissionfixed;
	IloExpr* emissionvar;
	IloExpr* transportemission;
	IloExpr* nop;
	IloExpr* now;

	

	
};

