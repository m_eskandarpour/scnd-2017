#ifndef GLOBALVar_H_GUARD
#define GLOBALVar_H_GUARD

#include <time.h>

extern float** obj1;
extern float** obj2;
extern float** durationTime;
extern clock_t tInit, tGoal;

#endif