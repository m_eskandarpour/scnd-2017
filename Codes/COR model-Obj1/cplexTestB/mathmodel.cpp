#include "cplexmod.h"
#include "mathmodel.h"


void cplexmod::Vardifinition()
{
	w = new IloIntVar*[plant];
	for(int i = 0; i < plant; i++)
	{
		w[i] = new IloIntVar(env,0,1);
	}
	h = new IloIntVar*[distribution];
	for(int i = 0; i < distribution; i++)
	{
		h[i] = new IloIntVar(env,0,1);
	}
	A = new IloIntVar***[supplier];
	for(int i = 0; i < supplier; i++)
	{
		A[i] = new IloIntVar**[plant];
		for(int j = 0; j < plant; j++)
		{
			A[i][j] = new IloIntVar*[transportation];
			for(int k = 0; k < transportation; k++)
				A[i][j][k] = new IloIntVar(env,0,1);
		}
	}
	B = new IloIntVar***[plant];
	for(int i = 0; i < plant; i++)
	{
		B[i] = new IloIntVar**[distribution];
		for(int j = 0; j < distribution; j++)
		{
			B[i][j] = new IloIntVar*[transportation];
			for(int k = 0; k < transportation; k++)
				B[i][j][k] = new IloIntVar(env,0,1);
		}
	}
	C = new IloIntVar***[distribution];
	for(int i = 0; i < distribution; i++)
	{
		C[i] = new IloIntVar**[customer];
		for(int j = 0; j < customer; j++)
		{
			C[i][j] = new IloIntVar*[transportation];
			for(int k = 0; k < transportation; k++)
				C[i][j][k] = new IloIntVar(env,0,1);
		}
	}

	r = new IloFloatVar****[distribution];
	for(int i = 0; i < distribution; i++)
	{
		r[i] = new IloFloatVar***[customer];
		for(int j = 0; j < customer; j++)
		{
			r[i][j] = new IloFloatVar**[transportation];
			for(int k = 0; k < transportation; k++)
			{
				r[i][j][k] = new IloFloatVar*[product];
				for(int l = 0; l < product; l++)
					r[i][j][k][l] = new IloFloatVar(env);
			}
		}
	}

	x = new IloFloatVar****[supplier];
	for(int i = 0; i < supplier; i++)
	{
		x[i] = new IloFloatVar***[plant];
		for(int j = 0; j < plant; j++)
		{
			x[i][j] = new IloFloatVar**[transportation];
			for(int k = 0; k < transportation; k++)
			{
				x[i][j][k] = new IloFloatVar*[product];
				for(int l = 0; l < product; l++)
					x[i][j][k][l] = new IloFloatVar(env);
			}
		}
	}

	y = new IloFloatVar****[plant];
	for(int i = 0; i < plant; i++)
	{
		y[i] = new IloFloatVar***[distribution];
		for(int j = 0; j < distribution; j++)
		{
			y[i][j] = new IloFloatVar**[transportation];
			for(int k = 0; k < transportation; k++)
			{
				y[i][j][k] = new IloFloatVar*[product];
				for(int l = 0; l < product; l++)
					y[i][j][k][l] = new IloFloatVar(env);
			}
		}
	}
}


void cplexmod::Constraints()
{
	//constraint related to epsilon
	IloExpr sumobj2(env);
for(int i = 0; i < supplier; i++)
		  for(int j = 0; j < plant; j++)
			  for(int k = 0; k < transportation; k++)
				  for(int l = 0; l < product; l++)
			  {
				  sumobj2 += *(x[i][j][k][l])*cep[i][j][k][l];
			  }

	for(int i = 0; i < plant; i++)
		  for(int j = 0; j < distribution; j++)
			  for(int k = 0; k < transportation; k++)
				  for(int l = 0; l < product; l++)
			  {
				  sumobj2 += *(y[i][j][k][l])*ced[i][j][k][l];
			  }
     for(int i = 0; i < distribution; i++)
		  for(int j = 0; j < customer; j++)
			  for(int k = 0; k < transportation; k++)
				  for(int l = 0; l < product; l++)
			  {
				  sumobj2 += *(r[i][j][k][l])*cec[i][j][k][l];
			  }
   for(int i = 0; i < plant; i++)
		sumobj2 += *(w[i])*fep[i];

	for(int i = 0; i < distribution; i++)
		sumobj2 += *(h[i])*fed[i];

	for(int i = 0; i < plant; i++)
		  for(int j = 0; j < distribution; j++)
			  for(int k = 0; k < transportation; k++)
				  for(int l = 0; l < product; l++)
				  {
					  sumobj2 += *(y[i][j][k][l])*vep[i];
				  }
     for(int i = 0; i < distribution; i++)
		  for(int j = 0; j < customer; j++)
			  for(int k = 0; k < transportation; k++)
				  for(int l = 0; l < product; l++)
				  {
					  sumobj2 += *(r[i][j][k][l])*ved[i];
				  }
			  IloAdd(*model, sumobj2 <=epsilon);
	//constraint 1
	for(int i = 0; i < supplier; i++)
	{
		
		IloExpr suppsum(env);
		for(int j = 0; j < plant; j++)
			  for(int k = 0; k < transportation; k++)
				  for(int l = 0; l < product; l++)
		
				  {
					  suppsum += *(x[i][j][k][l]);
				  }
		
			  IloAdd(*model, suppsum <=cas[i]);
		
			  //model.add(suppsum <=cas[i]);
	}
//constraint 2
	for(int i = 0; i < plant; i++)
	{
		IloExpr plantsum(env);
		for(int j = 0; j < distribution; j++)
			  for(int k = 0; k < transportation; k++)
				  for(int l = 0; l < product; l++)
				  {
					  plantsum += *(y[i][j][k][l]);
				  }
				  IloAdd(*model, plantsum <=*(w[i])*cap[i]);
			  //model.add(plantsum <=cap[i]*w[i]);
	}
//constraint 3    
	for(int i = 0; i < plant; i++)
	 {
		for(int l = 0; l < product; l++)
		{
		 IloExpr plantsum1(env);
		for(int j = 0; j < supplier; j++)
			  for(int k = 0; k < transportation; k++)
			  {
				  plantsum1 += *(x[j][i][k][l]);
			  }
		IloExpr plantsum2(env);
		for(int j = 0; j < distribution; j++)
			  for(int k = 0; k < transportation; k++)
			  {
				  plantsum2 += *(y[i][j][k][l]);
			  }	  
			  IloAdd(*model, plantsum1 ==plantsum2);
			  //model.add(plantsum1 ==plantsum2);
		}
	 }
//constraint 4
	IloExpr plantt(env);
		for(int j = 0; j < plant; j++)
			plantt += *(w[j]);
		IloAdd(*model, plantt <=up);
		//model.add(plantt <=up);

//constraint 5		
		
		for(int i = 0; i < distribution; i++)
			{
				IloExpr dissum(env);
				for(int j = 0; j < customer; j++)
					for(int k = 0; k < transportation; k++)
						for(int l = 0; l < product; l++)
					{
						dissum += *(r[i][j][k][l]);
					}
					IloAdd(*model, dissum <=*(h[i])*cad[i]);
					//model.add(dissum <=cad[i]*h[i]);
		    }
//constraint 6		
	for(int i = 0; i < distribution; i++)
		for(int l = 0; l < product; l++)
		 {
			IloExpr plantsum11(env);
			for(int j = 0; j < plant; j++)
				  for(int k = 0; k < transportation; k++)
				  {
					  plantsum11 += *(y[j][i][k][l]);
				  }
			IloExpr plantsum22(env);
			for(int j = 0; j < customer; j++)
				  for(int k = 0; k < transportation; k++)
				  {
					  plantsum22 += *(r[i][j][k][l]);
				  }	  
				  IloAdd(*model, plantsum11 ==plantsum22);
				  //model.add(plantsum11 ==plantsum22);
		 }
//constraint 7
	IloExpr distrib(env);
		for(int j = 0; j < distribution; j++)
			distrib += *(h[j]);
		IloAdd(*model, distrib <=ud);
		//model.add(distrib <=ud);
//constraint 8
for(int i = 0; i < customer; i++)
	for(int l = 0; l < product; l++)
	 {
		IloExpr custsum(env);
		for(int j = 0; j < distribution; j++)
			  for(int k = 0; k < transportation; k++)
			  {
				  custsum += *(r[j][i][k][l]);
			  }
			   IloAdd(*model, custsum ==demand[i][l]);
			  //model.add(custsum ==1);
    }
//for(int i = 0; i < customer; i++)
//	 {
//		IloExpr ctrans(env);
//		for(int j = 0; j < distribution; j++)
//			  for(int k = 0; k < transportation; k++)
//			  {
//				  ctrans += *(C[j][i][k]);
//			  }
//			   IloAdd(*model, ctrans ==1);
//			 
//    }
for(int i = 0; i < supplier; i++)
	{
		
		for(int j = 0; j < plant; j++)
		{
			IloExpr Atran(env);
			  for(int k = 0; k < transportation; k++)
			  {
				  Atran += *(A[i][j][k]);
			  }
			  IloAdd(*model, Atran <= 1);
		}
			  //model.add(plantsum <=cap[i]*w[i]);
	}
for(int i = 0; i < plant; i++)
	{
		
		for(int j = 0; j < distribution; j++)
		{
			IloExpr Btran(env);
			  for(int k = 0; k < transportation; k++)
			  {
				  Btran += *(B[i][j][k]);
			  }
			  IloAdd(*model, Btran <= 1);
		}
			  //model.add(plantsum <=cap[i]*w[i]);
	}
for(int i = 0; i < distribution; i++)
	 {
		
		for(int j = 0; j < customer; j++)
		{
			IloExpr Ctrans(env);
			  for(int k = 0; k < transportation; k++)
			  {
				  Ctrans += *(C[i][j][k]);
			  }
			   IloAdd(*model, Ctrans <=1);
		}
			  //model.add(custsum ==1);
    }
for(int i = 0; i < supplier; i++)
	{
		
		for(int j = 0; j < plant; j++)
		{
			
			  for(int k = 0; k < transportation; k++)
			  {
				  IloExpr Pxtrans(env);
				  for(int l = 0; l < product; l++)
					  Pxtrans += *(x[i][j][k][l]);
				  IloAdd(*model, Pxtrans >= *(A[i][j][k]));
			  }
			 
		}
			  //model.add(plantsum <=cap[i]*w[i]);
	}
for(int i = 0; i < plant; i++)
	{
		
		for(int j = 0; j < distribution; j++)
		{
			
			  for(int k = 0; k < transportation; k++)
			  {
				  IloExpr Pytrans(env);
				  for(int l = 0; l < product; l++)
					  Pytrans += *(y[i][j][k][l]);
				  IloAdd(*model, Pytrans >= *(B[i][j][k]));
			  }
			 
		}
			  //model.add(plantsum <=cap[i]*w[i]);
	}
for(int i = 0; i < distribution; i++)
	{
		
		for(int j = 0; j < customer; j++)
		{
			
			  for(int k = 0; k < transportation; k++)
			  {
				   IloExpr Prtrans(env);
				  for(int l = 0; l < product; l++)
					  Prtrans += *(r[i][j][k][l]);
				  IloAdd(*model, Prtrans >= *(C[i][j][k]));
			  }
			 
		}
			  //model.add(plantsum <=cap[i]*w[i]);
	}
for(int i = 0; i < supplier; i++)
	{
		
		for(int j = 0; j < plant; j++)
		{
			
			  for(int k = 0; k < transportation; k++)
			  {
				   IloExpr Pxctrans(env);
				  for(int l = 0; l < product; l++)
					  Pxctrans += *(x[i][j][k][l]);
				  IloAdd(*model, Pxctrans <= *(A[i][j][k])*cas[i]);
			  }
			 
		}
			  //model.add(plantsum <=cap[i]*w[i]);
	}
for(int i = 0; i < plant; i++)
	{
		
		for(int j = 0; j < distribution; j++)
		{
			
			  for(int k = 0; k < transportation; k++)
			  {
				  IloExpr Pyctrans(env);
				  for(int l = 0; l < product; l++)
					  Pyctrans += *(y[i][j][k][l]);
				  IloAdd(*model,  Pyctrans <= *(B[i][j][k])*cap[i]);
			  }
			 
		}
			  //model.add(plantsum <=cap[i]*w[i]);
	}
for(int i = 0; i < distribution; i++)
	{
		
		for(int j = 0; j < customer; j++)
		{
			
			  for(int k = 0; k < transportation; k++)
			  {
				  IloExpr Prctrans(env);
				  for(int l = 0; l < product; l++)
					  Prctrans += *(r[i][j][k][l]);
				  IloAdd(*model, Prctrans <= *(C[i][j][k])*cad[i]);
			  }
			 
		}
			  //model.add(plantsum <=cap[i]*w[i]);
	}
for(int i = 0; i < plant; i++)
	{
		IloExpr AWtrans(env);
		for(int j = 0; j < supplier; j++)
		{
			
			  for(int k = 0; k < transportation; k++)
			  {
				  AWtrans += *(A[j][i][k]);
				  
			  }
			 
		}
		IloAdd(*model, AWtrans >= *w[i]);
			  //model.add(plantsum <=cap[i]*w[i]);
	}
for(int i = 0; i < distribution; i++)
	{
		IloExpr BHtrans(env);
		for(int j = 0; j < plant; j++)
		{
			
			  for(int k = 0; k < transportation; k++)
			  {
				   BHtrans += *(B[j][i][k]);
				  
			  }
			 
		}
		IloAdd(*model, BHtrans >= *h[i]);
			  //model.add(plantsum <=cap[i]*w[i]);
	}

}






void cplexmod::objectivefunction()
{
objf = new IloExpr(env);
	for(int i = 0; i < supplier; i++)
		  for(int j = 0; j < plant; j++)
			  for(int k = 0; k < transportation; k++)
				  for(int l = 0; l < product; l++)
			  {
				  *objf += *(x[i][j][k][l])*tcp[i][j][k][l];
			  }

	for(int i = 0; i < plant; i++)
		  for(int j = 0; j < distribution; j++)
			  for(int k = 0; k < transportation; k++)
				  for(int l = 0; l < product; l++)
			  {
				  *objf += *(y[i][j][k][l])*tcd[i][j][k][l];
			  }
     for(int i = 0; i < distribution; i++)
		  for(int j = 0; j < customer; j++)
			  for(int k = 0; k < transportation; k++)
				  for(int l = 0; l < product; l++)
			  {
				  *objf += *(r[i][j][k][l])*tcc[i][j][k][l];
			  }
    for(int i = 0; i < plant; i++)
		*objf += *(w[i])*fcp[i];

	for(int i = 0; i < distribution; i++)
		*objf += *(h[i])*fcd[i];
	/*for(int i = 0; i < supplier; i++)
		  for(int j = 0; j < plant; j++)
			  for(int k = 0; k < transportation; k++)
			  for(int l = 0; l < product; l++)
			  {
				  *objf += *(x[i][j][k][l])*cep[i][j][k][l];
			  }

	for(int i = 0; i < plant; i++)
		  for(int j = 0; j < distribution; j++)
			  for(int k = 0; k < transportation; k++)
			  for(int l = 0; l < product; l++)
			  {
				  *objf += *(y[i][j][k][l])*ced[i][j][k][l];
			  }
     for(int i = 0; i < distribution; i++)
		  for(int j = 0; j < customer; j++)
			  for(int k = 0; k < transportation; k++)
			  for(int l = 0; l < product; l++)
			  {
				  *objf += *(r[i][j][k][l])*cec[i][j][k][l];
			  }
   for(int i = 0; i < plant; i++)
		*objf += *(w[i])*fep[i];

	for(int i = 0; i < distribution; i++)
		*objf += *(h[i])*fed[i];

	for(int i = 0; i < plant; i++)
		  for(int j = 0; j < distribution; j++)
			  for(int k = 0; k < transportation; k++)
			  for(int l = 0; l < product; l++)
			  {
				  *objf += *(y[i][j][k][l])*vep[i];
			  }
     for(int i = 0; i < distribution; i++)
		  for(int j = 0; j < customer; j++)
			  for(int k = 0; k < transportation; k++)
			  for(int l = 0; l < product; l++)
			  {
				  *objf += *(r[i][j][k][l])*ved[i];
			  }*/
	model->add(IloMinimize(env,*objf));
}