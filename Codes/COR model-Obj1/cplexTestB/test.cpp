#include "cplexmod.h"
#include <iostream>
#include <conio.h>
#include <fstream>
#include "GlobalVar.h"

#define Test 2
#define Senario 4
#define EpsilonN 1

using namespace std;
//ofstream out;

ofstream outfinal;

int main(int argc, char* argv[])
{
	int term1,term2=0,term3;
	obj1 = new float*[Test];
	for(int i=0; i<Test; i++)
		obj1[i] = new float[EpsilonN];
	obj2 = new float*[Test];
	for(int i=0; i<Test; i++)
		obj2[i] = new float[EpsilonN];
	durationTime= new float*[Test];
	for(int i=0; i<Test; i++)
		durationTime[i] = new float[EpsilonN];
	srand(time(0));
	
	clock_t initialtime;
	
	for(int i=0 ; i<Test ; i++)
	{
		term1=i;
		for(int i3=0 ; i3 < EpsilonN ; i3++)
		{
			term3=i3;
			tInit = clock();
		
			cplexmod* mod = new cplexmod(term1, term2,term3);//generate new pointer called mod of class cplexmod
			mod->mathMod();//(*mod).mathMod();

			mod->solveMod(term1, term2,term3);//(*mod).solveMod();
			delete mod;
			tGoal = clock();
			durationTime[i][i3] = ((double)tGoal-tInit)/CLOCKS_PER_SEC;
		}
	}
			
	
	outfinal.open("FinalResult-obj1" , ios::out);
	outfinal<<"Test"<<"\t"<<"objective 1"<<"\t"<<"objective 2"<<"\t"<<"CPU Time"<<"\n";
	for(int i=0; i< Test; i++)
		for(int j=0; j< EpsilonN; j++)
			outfinal<<i+1<<"\t"<<obj1[i][j]<<"\t"<<obj2[i][j]<<"\t"<<durationTime[i][j]<<"\n";

						
	//getch();
	return 0;
}
